# OpenML dataset: adult-census

https://www.openml.org/d/1119

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ronny Kohavi and Barry Becker  
**Source**: [MLRR](http://axon.cs.byu.edu:5000/)    
**Please cite**: Ron Kohavi, "Scaling Up the Accuracy of Naive-Bayes Classifiers: a Decision-Tree Hybrid", Proceedings of the Second International Conference on Knowledge Discovery and Data Mining, 1996  

Dataset from the MLRR repository: http://axon.cs.byu.edu:5000/

**Note: this dataset is identical to the version stored in UCI, but only includes the training data, not the test data. See [adult (2)](http://openml.org/d/1590) for the complete data.**

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1119) of an [OpenML dataset](https://www.openml.org/d/1119). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1119/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1119/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1119/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

